# Frontend Mentor - Blog preview card solution

This is a solution to the [Blog preview card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/blog-preview-card-ckPaj01IcS). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to: 

- See hover and focus states for all interactive elements on the page

### Screenshot

![](./Screenshot 2024-05-16 at 15-25-51 Frontend Mentor Blog preview card.png)

### Links

- Solution URL: [GitLab Project](https://gitlab.com/lampros-liontos-frontend-mentor-projects/blog-preview-card)
- Live Site URL: [Live Site on Netlify](https://ll-fm-blog-preview-card.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox

### What I learned

I took some of the things I learned from the previous project, particularly regarding the `@font-face`, `*` and `:root` elements, and tried them out here, to a satisfactory conclusion.  I also learned better how flexboxes worked, and how to properly refine the HTML to make the most of them, particularly where container placement is concerned.  I already had some knowledge about `:hover`, so that was pretty straightforward.

### Continued development

I definitely plan to get more practice in on organizing my HTML code in order to make maximum use of the flexbox functionality, making it easier to align and position elements on the page without struggling to learn why something is not centering (something I was struggling with the two projects so far).

### Useful resources

Again, I mostly made use of the following three references:
-[OpenAI ChatGPT]() - This continues to be a useful tool for finding the right place to begin my research.  I know a lot of people pooh-pooh AI, but it helps when you think of it as a reference assistant... it provides a good starting point for which to search out more and better answers.
- [W3 Schools](https://www.w3schools.com) - When I need a good introduction to a concept, W3 Schools is an incredible resource.  This site includes both reference materials, as well as a place to experiment with the various commands to really get an understanding going.
- [Developer Documentation](https://devdocs.io/) - One of the nice things about devdocs is that it is not only useful as a website, but it also makes its documentation archive available for download to use as a local archive.  While it's similar in purpose to W3 Schools, it's closer to a reference than a training site.

## Author

- Frontend Mentor - [@reteov](https://www.frontendmentor.io/profile/reteov)
- Twitter - [@reteov](https://www.twitter.com/reteov)

## Acknowledgments

I have to thank Sandy Astorga for the `@font-face`, `*`, and `:root` elements; I would not have figured those out otherwise.
